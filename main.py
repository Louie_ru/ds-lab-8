from multiprocessing import Process, Pipe
from time import sleep


def event(pid, counter):
    counter[pid] += 1
    print(f'Event in {pid} with counter {counter}')
    return counter


def send_message(pipe, pid, counter):
    counter[pid] += 1
    pipe.send(('Hello', counter))
    print(f'Message sent in {pid} with counter {counter}')
    return counter


def recv_message(pipe, pid, counter):
    counter[pid] += 1
    message, timestamp = pipe.recv()
    for i in range(len(counter)):
        counter[i] = max(timestamp[i], counter[i])
    print(f'Message received in {pid} with counter {counter}')
    return counter


def process_a(a_b):
    pid = 0
    counter = [0, 0, 0]
    counter = send_message(a_b, pid, counter)
    counter = send_message(a_b, pid, counter)
    counter = event(pid, counter)
    counter = recv_message(a_b, pid, counter)
    counter = event(pid, counter)
    counter = event(pid, counter)
    counter = recv_message(a_b, pid, counter)
    sleep(0.1)
    print(f"Process a finished with counter {counter}")


def process_b(b_a, b_c):
    pid = 1
    counter = [0, 0, 0]
    counter = recv_message(b_a, pid, counter)
    counter = recv_message(b_a, pid, counter)
    counter = send_message(b_a, pid, counter)
    counter = recv_message(b_c, pid, counter)
    counter = event(pid, counter)
    counter = send_message(b_a, pid, counter)
    counter = send_message(b_c, pid, counter)
    counter = send_message(b_c, pid, counter)
    sleep(0.1)
    print(f"Process b finished with counter {counter}")


def process_c(c_b):
    pid = 2
    counter = [0, 0, 0]
    counter = send_message(c_b, pid, counter)
    counter = recv_message(c_b, pid, counter)
    counter = event(pid, counter)
    counter = recv_message(c_b, pid, counter)
    sleep(0.1)
    print(f"Process c finished with counter {counter}")


if __name__ == '__main__':
    a_b, b_a = Pipe()
    b_c, c_b = Pipe()

    a_process = Process(target=process_a, args=(a_b,))
    b_process = Process(target=process_b, args=(b_a, b_c))
    c_process = Process(target=process_c, args=(c_b,))
    a_process.start()
    b_process.start()
    c_process.start()
    a_process.join()
    b_process.join()
    c_process.join()
